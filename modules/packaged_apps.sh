#!/usr/bin/env bash

MODULE_NAME='Packaged applications'

module_packages \
    ack-grep \
    automake \
    build-essential \
    cmake \
    cscope \
    exuberant-ctags \
    g++ \
    libtool \
    openssh-server \
    python3-pip \
    stow \
    xclip \
